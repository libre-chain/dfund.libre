# daofund.Libre

The primary function is to manage contributions of pBTC, stakes, and distributions of LIBRE tokens over a specified period. Key functionalities include tracking PBTC contributions, allocating LIBRE tokens based on contributions, creating stakes of LIBRE, then claims the stakes, and distributing LIBRE tokens to contributors after a 6-month lockup period. This smart contract ensures fair and transparent distribution of LIBRE tokens and provides users with the ability to check and claim their stakes. It serves as a mechanism for incentivizing contributions and engagement within the Libre blockchain ecosystem.

## Dependencies

- Unique issuance of 600 million Libre tokens allocated by the validators / DAO to be distributed into stakes by `daofund.libre` at a rate of 10 million Libre tokens per day for 60 days in exchange for however much BTC is contributed that day.
- **stake.libre** contract must exist.
- A cron job must be set up to trigger the **process**, **stake**, and **checkclaim** actions.

## Specifications

- Understand 6 months as a period of 30 days each month.
- Contributions will be accepted for the first 60 days after the initial date.
- Accept **PBTC** contributions using the **btc.ptokens** contract. Any other transaction with a different token symbol or contract origin will be rejected.
- Allocate LIBRE tokens based on daily contributions. This process occurs at the end of each 24-hour slot.
- Create new stakes using the **stake.libre** contract at the end of each time slot.
- Any amount leftover from an allocation process will remain in the contract and will not be considered for any other further allocation.
- If any other donation other than **the 600 million LIBRE** are transferred to the contract, will not count for the allocation, but instead will go to the DAO at the end of the entire 60 day period.
- The contributed PBTC will be sent to dao.libre each 24-hour slot. Understand a 24-hour slot as, from 00:00:00 to 23:59:59.
- After the 6-month lockup period from the Smart Contract starting contribution time:
  - Every 24 hours, _claim_ any stakes that are available.
  - Every 24 hours, _distribute_ **claimed LIBRE tokens** to contributors.

## User flow

We have two accounts, **alice** and **bob**.

- On day 1, both **alice** and **bob** contribute **0.1 BTC**, which goes into the contribution table.
- At the end of the day, run _process_ and all of the BTC contributions are counted and LIBRE is allocated in the stakes table.
- At the end of each day, the PBTC contributions will be sent to dao.libre.
- Each of them receives **5 million LIBRE** tokens allocated in the stakes table.
- These tokens are subsequently staked in **stake.libre** by running **stake** action.
- They are staked for a period of 6 months, and **the stakes are owned by daofund**.
- The **daofund.libre** can run **claim** daily to see if there are any claims available on **stake.libre** for the stakes it made.
- If a stake can be claimed, then it is updated in the **contributions** table and can be distributed back to the original account who contributed.

## Key Structure

<table>
  <tr>
   <td><strong>Permission</strong>
   </td>
   <td><strong>Threshold</strong>
   </td>
   <td colspan="2" ><strong>Authority</strong>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td><strong>accounts/keys</strong>
   </td>
   <td><strong>weights</strong>
   </td>
  </tr>
  <tr>
   <td>owner
   </td>
   <td><p style="text-align: right">
1</p>

   </td>
   <td>board.libre
   </td>
   <td>+1
   </td>
  </tr>
  <tr>
   <td>active
   </td>
   <td><p style="text-align: right">
1</p>

   </td>
   <td>board.libre
   </td>
   <td>+1
   </td>
  </tr>
  <tr>
   <td>eosio@code
   </td>
   <td><p style="text-align: right">
1</p>

   </td>
   <td>daofund.libre
   </td>
   <td>+1
   </td>
  </tr>
</table>

## Actions

The following list outlines the contract interactions for a user.

### on_transfer (notify)

This function serves as an event listener and, while not an action on its own, it acts as the primary trigger to initiate the contract flow. It monitors transfer events originating from the **btc.ptokens** contract with the **PBTC** token symbol.

#### Parameters

- from: account sender the contribution.
- to: daofund.libre.
- quantity: **PBTC** contribution amount.
- memo: optional field to specify the purpose of the transfer.

### init

Specify the initial time for accepting contributions in the contract.

#### Parameters

- start_date: Time for when the contract will start accepting contributions.
- days: Amount of days the contract will be allowing contributions.

### process

Count **PBTC** contributions and allocate daily LIBRE tokens.

#### Parameters

- limit: int - Maximum number of table rows to process in a single execution.

### stake

Create new stakes for 6 months for each new contribution that occurred during the same slot time of 24 hours.

#### Parameters

- limit: int - Maximum number of table rows to process in a single execution.

### checkclaim

Check if there are stakes to be claimed.

#### Parameters

- limit: Maximum number of table rows to check in a single execution.

### withdraw

Distribute LIBRE tokens to contributors.

#### Parameters

- account: Name of the owner of the earned staked funds.

## Tables

The necessary tables to fulfill the contract's requirements are as follows:

### global (singleton)

Track global parameters to determine the starting date for enabling contract actions and other functionalities.

<img src="./docs/table_global.png" alt="global table" width="361" height="207">

### contribution

Store contributions made during the 24-hour slot period and allocate the corresponding amount of LIBRE rewards for each contribution at the end of the 6-month lockup period.

<img src="./docs/table_contribution.png" alt="contribution table" width="379" height="299">

#### Secondary Indexes

- byaccount: filter by contributor.
- bycontrdate: filter by date the contribution was made.
- bystatus: filter by pending, ready_to_claim and claimed.

### stakeprocess (singleton)

Manage calculation processes and operations to determine the daily LIBRE allocation per account.

<img src="./docs/table_stakeprocess.png" alt="stakeprocess table" width="650" height="161">

### distribution

Store the total reward amount per account.

<img src="./docs/table_distribution.png" alt="distribution table" width="379" height="125">

## Diagrams

The following aims to give an overview of the **daofund** contract.

### Activity

<img src="./docs/diagram_activity.png" alt="daofund activity diagram" width="206" height="536">
