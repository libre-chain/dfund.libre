#pragma once

#include <eosio/asset.hpp>
#include <eosio/name.hpp>

namespace libre {
  inline constexpr uint16_t ONE_YEAR_DAYS = 365;
  inline constexpr uint16_t MAX_STAKE_LENGTH_DAYS = 4 * ONE_YEAR_DAYS;
  inline constexpr uint16_t MIN_STAKE_LENGTH_DAYS = 1;
  inline constexpr uint16_t MAX_MINT_LENGTH_DAYS = 4 * ONE_YEAR_DAYS;
  inline constexpr uint16_t MIN_MINT_LENGTH_DAYS = 1;

  inline const std::string DONATION_TRANSFER = "donation";

  inline constexpr eosio::name SUPPORTED_TOKEN_CONTRACT =
      eosio::name( "eosio.token" );
  inline constexpr eosio::symbol SUPPORTED_TOKEN_SYMBOL =
      eosio::symbol( "LIBRE", 4 );
  inline constexpr eosio::name TOKEN_ISSUER = eosio::name( "eosio" );
  inline constexpr uint64_t SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER = 10000;
  inline constexpr eosio::name SUPPORTED_MINT_TOKEN_CONTRACT =
      eosio::name( "btc.ptokens" );
  inline constexpr eosio::symbol SUPPORTED_MINT_TOKEN_SYMBOL =
      eosio::symbol( "PBTC", 9 );
  inline constexpr eosio::name DAO_ACCOUNT = eosio::name( "dao.libre" );
  inline constexpr eosio::name DEFAULT_REFERRER_ACCOUNT =
      eosio::name( "bitcoinlibre" );
  inline constexpr uint8_t INFLATION_PCT = 10;
  inline constexpr double  UNSTAKE_PENALTY = 0.2;
  inline constexpr double  ALPHA0 = 0.1;  // minimum yield
  inline constexpr double  BETA0 = 2.0;   // maximum yield
  inline constexpr double  ALPHAT = 0.06; // terminal min yield
  inline constexpr double  BETAT = 0.3;   // terminal max yield
  inline constexpr double  T = 730.0;     // days to terminal yield
  inline const auto        S_L =
      eosio::time_point_sec( 1656892800 ); // Launch Date 2022-07-04 GMT
  inline const auto TOTAL_LIBRE_TOKEN =
      eosio::asset( 200000000'0000, SUPPORTED_TOKEN_SYMBOL );

  inline constexpr auto EOSIO_CONTRACT = eosio::name( "eosio" );
  inline constexpr auto EOSIO_CONTRACT_ACTION = eosio::name( "vonstake" );
} // namespace libre