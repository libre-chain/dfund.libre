#include <daofund.hpp>

namespace daofund {
  void daofund::notify_transfer( eosio::name  &from,
                                 eosio::name  &to,
                                 eosio::asset &quantity,
                                 std::string  &memo ) {
    // tokens from the contract
    if ( from == get_self() ) {
      sub_balance( to, quantity );

      return;
    }

    // skip transactions that are not related
    if ( to != get_self() ) {
      return;
    }

    if ( SUPPORTED_TOKEN_LIBRE_CONTRACT == get_first_receiver() &&
         quantity.symbol == SUPPORTED_TOKEN_LIBRE_SYMBOL ) {
      set_global( quantity );

      return;
    }

    if ( SUPPORTED_TOKEN_BTC_CONTRACT == get_first_receiver() ) {
      eosio::check( quantity.symbol == SUPPORTED_TOKEN_BTC_SYMBOL,
                    "invalid symbol, expected: " +
                        SUPPORTED_TOKEN_BTC_SYMBOL.code().to_string() );

      add_contribution( from, quantity );

      return;
    }

    eosio::check( false, "invalid token contract" );
  }

  void daofund::init( eosio::time_point_sec &start_date, uint8_t total_days ) {
    require_auth( get_self() );

    auto global = global_sing.get_or_default();

    eosio::check( global.total_amount.amount == TOTAL_LIBRE_FUNDING.amount,
                  "contract must be funded before initialization" );
    eosio::check( total_days == CONTRACT_ACTIVE_TOTAL_DAYS,
                  "expected total days: " +
                      std::to_string( CONTRACT_ACTIVE_TOTAL_DAYS ) );
    eosio::check( start_date >=
                      eosio::time_point_sec( eosio::current_time_point() ),
                  "start date must be in the future" );

    global.start_date = start_date;
    global.end_date =
        start_date + eosio::days( static_cast< int64_t >( total_days ) );
    global.allocation_per_day = global.total_amount / total_days;

    global_sing.set( global, get_self() );
    stakeprocess_sing.get_or_create(
        get_self(),
        stakeprocess_standby{ .next_contribution_date =
                                  start_date +
                                  eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
                              .next_stake_index = START_STAKE_INDEX_LOOKUP,
                              .next_index_cache = 0 } );
  }

  void daofund::process( uint16_t limit ) {
    uint16_t limit_passed = limit;

    auto global = global_sing.get_or_default();

    eosio::check( global.end_date != eosio::time_point_sec(),
                  "contract not initialized" );

    auto stakeprocess = stakeprocess_sing.get_or_default();

    if ( auto *process =
             std::get_if< stakeprocess_standby >( &stakeprocess ) ) {

      if ( global.end_date < process->next_contribution_date ) {
        stakeprocess = stakeprocess_claim{
            .contribution_date = process->next_contribution_date,
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };
      } else {
        eosio::check( eosio::time_point_sec( eosio::current_time_point() ) >=
                          process->next_contribution_date,
                      "not ready to process" );

        stakeprocess = stakeprocess_count{
            .contribution_date = process->next_contribution_date,
            .next_index = process->next_index_cache,
            .total_pbtc_contributed =
                eosio::asset( 0, SUPPORTED_TOKEN_BTC_SYMBOL ),
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };
      }

      stakeprocess_sing.set( stakeprocess, get_self() );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_count >( &stakeprocess ) ) {
      auto copy = *process;
      process_count( limit_passed, copy );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process =
             std::get_if< stakeprocess_allocate >( &stakeprocess ) ) {
      auto copy = *process;
      process_allocate( limit_passed, copy );

      // return is made on purpose because it is required the stakes are proccessed by
      // the stake.libre contract
      return;
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_link >( &stakeprocess ) ) {
      auto copy = *process;
      process_link( limit_passed, copy );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_claim >( &stakeprocess ) ) {
      eosio::time_point_sec ctp_sec =
          eosio::time_point_sec( eosio::current_time_point() );

      eosio::time_point_sec start_claiming_time =
          global.start_date + eosio::days( DEFAULT_TOTAL_STAKE_DAYS );

      if ( ctp_sec > start_claiming_time ) {
        auto copy = *process;
        process_claim( limit_passed, copy );
      } else {
        auto sp = stakeprocess_sing.get();

        sp = stakeprocess_standby{
            .next_contribution_date =
                process->contribution_date +
                eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };

        --limit_passed;

        stakeprocess_sing.set( sp, get_self() );
      }
    }

#ifndef BUILD_TESTNET
    // this is for mainnet only.
    // contract was developed considering that stakes are going to be avaiable to claim after 180 days
    // from the start contribution date, so, this way, the scenario where a contribution date is lower than the
    // current contribution time shouldn't exist unless end contribution period > start contribution period + DEFAULT_TOTAL_STAKE_DAYS.
    eosio::check( limit_passed != limit, "nothing to process" );
#endif
  }

  void daofund::withdraw( uint64_t index ) {
    auto curr_contr = contribution_tb.find( index );

    eosio::check( curr_contr != contribution_tb.end(),
                  "contribution not found" );
    eosio::check( curr_contr->status == e_stake_status::CLAIMED,
                  "contribution is not ready to be claimed" );

    require_auth( curr_contr->account );

    add_balance( curr_contr->account, curr_contr->stake_payout );

    std::string memo = "withdraw funds from: " + get_self().to_string();

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_LIBRE_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    curr_contr->account,
                                    curr_contr->stake_payout,
                                    memo ) )
        .send();

    contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
      row.status = e_stake_status::WITHDRAWN;
    } );
  }

  // private methods
  void daofund::set_global( eosio::asset &quantity ) {
    if ( global_sing.exists() ) {
      return;
    }

    eosio::check( quantity.amount == TOTAL_LIBRE_FUNDING.amount,
                  "invalid amount, expected: " +
                      TOTAL_LIBRE_FUNDING.to_string() );
    eosio::check( quantity.symbol == SUPPORTED_TOKEN_LIBRE_SYMBOL,
                  "invalid symbol, expected: " +
                      SUPPORTED_TOKEN_LIBRE_SYMBOL.code().to_string() );

    auto glb = global_sing.get_or_create( get_self(), global{} );

    glb.total_amount = quantity;
    glb.total_amount_allocated =
        eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );

    global_sing.set( glb, get_self() );
  }

  void daofund::add_contribution( eosio::name  &account,
                                  eosio::asset &quantity ) {
    auto global = global_sing.get_or_default();

    eosio::check( global.end_date != eosio::time_point_sec(),
                  "contract not initialized" );
    eosio::check( eosio::time_point_sec( eosio::current_time_point() ) >=
                      global.start_date,
                  "contribution period not open yet" );
    eosio::check( eosio::time_point_sec( eosio::current_time_point() ) <
                      global.end_date,
                  "contribution period has ended" );

    contribution_tb.emplace( get_self(), [&]( auto &row ) {
      row.index = contribution_tb.available_primary_key();
      row.account = account;
      row.pbtc_amount = quantity;
      row.contr_date = eosio::current_time_point();
      row.stake_amount = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
      row.stake_payout = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
      row.stake_date = eosio::time_point_sec();
      row.status = e_stake_status::PENDING;
    } );
  }

  void daofund::process_count( uint16_t           &limit,
                               stakeprocess_count &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto end_contr = contribution_tb.end();

    for ( ;
          curr_contr != end_contr &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status == e_stake_status::PENDING &&
           curr_contr->contr_date >= start_slot_time ) {
        stakeprocess.total_pbtc_contributed += curr_contr->pbtc_amount;
      }
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr != end_contr &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      process = stakeprocess;
    } else {
      auto global = global_sing.get();

      process = stakeprocess_allocate{
          .contribution_date = stakeprocess.contribution_date,
          .next_index = stakeprocess.next_index_cache,
          .total_pbtc_contributed = stakeprocess.total_pbtc_contributed,
          .amount_allocated = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL ),
          .total_amount_to_allocate = global.allocation_per_day,
          .next_stake_index = stakeprocess.next_stake_index,
          .next_index_cache = stakeprocess.next_index_cache };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void daofund::process_allocate( uint16_t              &limit,
                                  stakeprocess_allocate &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );
    auto global = global_sing.get_or_default();
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto end_contr = contribution_tb.end();

    for ( ;
          curr_contr != end_contr &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status != e_stake_status::PENDING ||
           curr_contr->contr_date < start_slot_time ) {
        continue;
      }

      auto stake_amount =
          calculate_allocation_reward( curr_contr->pbtc_amount,
                                       stakeprocess.total_pbtc_contributed,
                                       stakeprocess.total_amount_to_allocate );

      if ( stake_amount.amount > 0 ) {
        add_balance( SUPPORTED_STAKING_CONTRACT, stake_amount );

        std::string memo =
            "stakefor:" + std::to_string( DEFAULT_TOTAL_STAKE_DAYS );

        eosio::action( eosio::permission_level{ get_self(), "active"_n },
                       SUPPORTED_TOKEN_LIBRE_CONTRACT,
                       "transfer"_n,
                       std::make_tuple( get_self(),
                                        SUPPORTED_STAKING_CONTRACT,
                                        stake_amount,
                                        memo ) )
            .send();
      }

      contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.stake_amount = stake_amount;
        row.status = stake_amount.amount > 0
                         ? e_stake_status::PENDING_LINK_STAKE
                         : e_stake_status::FROZEN;
      } );

      global.total_amount_allocated += stake_amount;
      stakeprocess.amount_allocated += stake_amount;
    }

    global_sing.set( global, get_self() );

    auto process = stakeprocess_sing.get();

    if ( curr_contr != end_contr &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      process = stakeprocess;
    } else {
      if ( stakeprocess.total_amount_to_allocate.amount >
           stakeprocess.amount_allocated.amount ) {
        send_leftofver_allocation( stakeprocess.total_amount_to_allocate -
                                       stakeprocess.amount_allocated,
                                   stakeprocess.contribution_date );

        stakeprocess.amount_allocated.amount = 0;
      }

      if ( stakeprocess.total_pbtc_contributed.amount > 0 ) {
        send_daily_pbtc_contribution( stakeprocess.total_pbtc_contributed,
                                      stakeprocess.contribution_date );
      }

      process = stakeprocess_link{
          .next_index = stakeprocess.next_index_cache,
          .next_stake_index = stakeprocess.next_stake_index,
          .contribution_date = stakeprocess.contribution_date,
          .next_index_cache = stakeprocess.next_index_cache,
          .new_contrs = stakeprocess.total_pbtc_contributed.amount > 0 };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void daofund::process_link( uint16_t          &limit,
                              stakeprocess_link &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );

    libre::stake_table stake_tb( SUPPORTED_STAKING_CONTRACT,
                                 SUPPORTED_STAKING_CONTRACT.value );
    auto stake_itr = stake_tb.lower_bound( stakeprocess.next_stake_index );
    auto stake_itr_end = stake_tb.end();
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto contr_end = contribution_tb.end();

    for ( ;
          curr_contr != contr_end &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status != e_stake_status::PENDING_LINK_STAKE ||
           curr_contr->contr_date < start_slot_time ) {
        continue;
      }

      while ( stake_itr != stake_itr_end && limit > 0 ) {
        if ( stake_itr->index >= stakeprocess.next_stake_index &&
             stake_itr->account == get_self() &&
             stake_itr->libre_staked == curr_contr->stake_amount &&
             stake_itr->stake_date >= curr_contr->contr_date ) {
          break;
        }

        --limit;
        ++stake_itr;
      }

      if ( limit == 0 ) {
        break;
      }

      if ( stake_itr == stake_itr_end ) {
        eosio::check( false, "stake corrupted: not found" );
      }

      stakeprocess.next_stake_index = stake_itr->index;

      contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.stake_index = stake_itr->index;
        row.stake_date = stake_itr->stake_date;
        row.stake_payout = stake_itr->payout;
        row.status = e_stake_status::STAKE_LINKED;
      } );

      stakeprocess.next_index = curr_contr->index;
      ++stake_itr;
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr != contr_end &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      stakeprocess.next_stake_index = stake_itr->index;
      process = stakeprocess;
    } else {
      process = stakeprocess_claim{
          .contribution_date = stakeprocess.contribution_date,
          .next_stake_index = stakeprocess.next_stake_index +
                              ( stakeprocess.new_contrs ? 1 : 0 ),
          .next_index_cache =
              stakeprocess.next_index + ( stakeprocess.new_contrs ? 1 : 0 ) };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void daofund::process_claim( uint16_t           &limit,
                               stakeprocess_claim &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto ctp = eosio::current_time_point();
    auto end_slot_time =
        eosio::time_point_sec( ctp - eosio::days( DEFAULT_TOTAL_STAKE_DAYS ) );
    auto contr_bystatus_index = contribution_tb.get_index< "bystatus"_n >();
    auto indexby_value =
        static_cast< uint64_t >( e_stake_status::STAKE_LINKED );
    auto curr_contr = contr_bystatus_index.lower_bound( indexby_value );
    auto end_contr = contr_bystatus_index.upper_bound( indexby_value );

    // stakingtoken.cpp - L:88
    // check( row->payout_date.sec_since_epoch() <
    //            eosio::current_time_point().sec_since_epoch(),
    //        "invalid date, not yet ready to claim" );
    for ( ; curr_contr != end_contr && curr_contr->stake_date < end_slot_time &&
            limit > 0;
          --limit ) {
      eosio::action( eosio::permission_level{ get_self(), "active"_n },
                     SUPPORTED_STAKING_CONTRACT,
                     "claim"_n,
                     std::make_tuple( get_self(), curr_contr->stake_index ) )
          .send();

      contr_bystatus_index.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.status = e_stake_status::CLAIMED;
      } );

      curr_contr = contr_bystatus_index.lower_bound( indexby_value );
      end_contr = contr_bystatus_index.upper_bound( indexby_value );
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr == end_contr || curr_contr->stake_date >= end_slot_time ) {
      process = stakeprocess_standby{
          .next_contribution_date = stakeprocess.contribution_date +
                                    eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
          .next_stake_index = stakeprocess.next_stake_index,
          .next_index_cache = stakeprocess.next_index_cache };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  eosio::asset daofund::calculate_allocation_reward(
      const eosio::asset &pbtc_contributed,
      const eosio::asset &total_pbtc_contributed,
      const eosio::asset &total_allocation_per_day ) {
    double contr_percent = static_cast< double >( pbtc_contributed.amount ) /
                           total_pbtc_contributed.amount;

    return eosio::asset( contr_percent * total_allocation_per_day.amount,
                         SUPPORTED_TOKEN_LIBRE_SYMBOL );
  }

  void daofund::send_leftofver_allocation(
      const eosio::asset          &amount,
      const eosio::time_point_sec &contribution_date ) {
    add_balance( SUPPORTED_DAO_CONTRACT, amount );

    std::string leftover_funds_memo =
        DAO_FOUNDING_TRANSFER_MEMO + ": leftover funds";

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_LIBRE_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    SUPPORTED_DAO_CONTRACT,
                                    amount,
                                    leftover_funds_memo ) )
        .send();
  }

  void daofund::send_daily_pbtc_contribution(
      const eosio::asset          &amount,
      const eosio::time_point_sec &contribution_date ) {
    add_balance( SUPPORTED_DAO_CONTRACT, amount );

    std::string pbtc_contribution_memo =
        DAO_FOUNDING_TRANSFER_MEMO + ": " +
        SUPPORTED_TOKEN_BTC_SYMBOL.code().to_string() + " " +
        get_self().to_string() + " contribution on " +
        std::to_string( contribution_date.sec_since_epoch() );

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_BTC_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    SUPPORTED_DAO_CONTRACT,
                                    amount,
                                    pbtc_contribution_memo ) )
        .send();
  }

  void daofund::add_balance( const eosio::name  &account,
                             const eosio::asset &quantity ) {
    distribution_table_type distribution_tb( get_self(),
                                             quantity.symbol.code().raw() );
    auto distribution_itr = distribution_tb.find( account.value );

    if ( distribution_itr == distribution_tb.end() ) {
      distribution_tb.emplace( get_self(), [&]( auto &row ) {
        row.account = account;
        row.reward_amount = quantity;
        row.total_claimed = quantity;
      } );
    } else {
      distribution_tb.modify( distribution_itr, get_self(), [&]( auto &row ) {
        row.reward_amount += quantity;
        row.total_claimed += quantity;
      } );
    }
  }

  void daofund::sub_balance( const eosio::name  &account,
                             const eosio::asset &quantity ) {
    distribution_table_type distribution_tb( get_self(),
                                             quantity.symbol.code().raw() );
    auto distribution_itr = distribution_tb.find( account.value );

    eosio::check( distribution_itr != distribution_tb.end(),
                  "account does not have a balance" );
    eosio::check( distribution_itr->reward_amount >= quantity,
                  "overdrawn balance" );

    distribution_tb.modify( distribution_itr, get_self(), [&]( auto &row ) {
      row.reward_amount -= quantity;
    } );
  }

} // namespace daofund

EOSIO_ACTION_DISPATCHER( daofund::actions )
EOSIO_ABIGEN( actions( daofund::actions ),
              table( "global"_n, daofund::global ),
              table( "contribution"_n, daofund::contribution ),
              table( "stakeprocess"_n, daofund::stakeprocess_variant ),
              table( "distribution"_n, daofund::distribution ) )