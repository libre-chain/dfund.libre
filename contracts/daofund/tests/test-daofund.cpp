#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

#ifndef BUILD_TESTNET

TEST_CASE( "init daofnd contract" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  expect( t.alice.trace< daofund::actions::init >(
              eosio::time_point_sec{ 1696118400 },
              60 ),
          "missing authority of daofnd.libre" );
  expect( t.daofund.trace< daofund::actions::init >(
              eosio::time_point_sec{ 1696118400 },
              60 ),
          "assertion failure with message: contract must be funded before "
          "initialization" );

  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "daofnd.libre"_n,
                                                 s2a( "600000000.0000 LIBRE" ),
                                                 "funding account" );
  t.chain.start_block();

  daofund::global expected_global = {
      .start_date = eosio::time_point_sec{},
      .end_date = eosio::time_point_sec{},
      .allocation_per_day = eosio::asset{},
      .total_amount = s2a( "600000000.0000 LIBRE" ),
      .total_amount_allocated = s2a( "0.0000 LIBRE" ) };

  auto global =
      t.get_singleton< daofund::global_singleton_type, daofund::global >();

  CHECK( global.start_date == expected_global.start_date );
  CHECK( global.end_date == expected_global.end_date );
  CHECK( global.allocation_per_day == expected_global.allocation_per_day );
  CHECK( global.total_amount == expected_global.total_amount );
  CHECK( global.total_amount_allocated ==
         expected_global.total_amount_allocated );

  expect( t.daofund.trace< daofund::actions::init >(
              eosio::time_point_sec{ 1696118400 },
              59 ),
          "assertion failure with message: expected total days: 60" );
  expect( t.daofund.trace< daofund::actions::init >(
              eosio::time_point_sec{ 1696118399 },
              60 ),
          "assertion failure with message: start date must be in the future" );

  t.daofund.act< daofund::actions::init >( eosio::time_point_sec{ 1696118400 },
                                           60 );

  global = t.get_singleton< daofund::global_singleton_type, daofund::global >();
  expected_global.start_date = eosio::time_point_sec{ 1696118400 };
  expected_global.end_date = eosio::time_point_sec{ 1696118400 + 60 * 86400 };
  expected_global.allocation_per_day = s2a( "10000000.0000 LIBRE" );

  CHECK( global.start_date == expected_global.start_date );
  CHECK( global.end_date == expected_global.end_date );
  CHECK( global.allocation_per_day == expected_global.allocation_per_day );
}

TEST_CASE( "add contribution" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "daofnd.libre"_n,
                                                  s2a( "0.100000000 PBTC" ),
                                                  "contribute on day 1" ),
          "contract not initialized" );

  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "daofnd.libre"_n,
                                                  s2a( "0.100000000 PBTC" ),
                                                  "contribute on day 1" ),
          "contribution period not open yet" );

  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.100000000 PBTC" ),
                                        "contribute on day 1" );

  daofund::contribution expected_contribution = {
      .index = 0,
      .account = "alice"_n,
      .pbtc_amount = s2a( "0.100000000 PBTC" ),
      .contr_date = eosio::time_point_sec{ 1696118400 + 1 * 86400 },
      .stake_index = 0,
      .stake_amount = s2a( "0.0000 LIBRE" ),
      .stake_date = eosio::time_point_sec{},
      .status = daofund::e_stake_status::PENDING };

  auto contribution = t.get_contribution( 0 );

  CHECK( contribution.index == expected_contribution.index );
  CHECK( contribution.account == expected_contribution.account );
  CHECK( contribution.pbtc_amount == expected_contribution.pbtc_amount );
  CHECK( contribution.contr_date == expected_contribution.contr_date );
  CHECK( contribution.stake_index == expected_contribution.stake_index );
  CHECK( contribution.stake_amount == expected_contribution.stake_amount );
  CHECK( contribution.stake_date == expected_contribution.stake_date );
  CHECK( contribution.status == expected_contribution.status );

  t.skip_to( "2023-11-29T23:59:59.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.100000000 PBTC" ),
                                        "contribute on day 1" );

  CHECK( t.get_contribution( 1 ).contr_date != eosio::time_point_sec{} );

  t.skip_to( "2023-12-01T00:00:00.000" );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "daofnd.libre"_n,
                                                  s2a( "0.100000000 PBTC" ),
                                                  "contribute on day 1" ),
          "contribution period has ended" );
}

TEST_CASE( "default stake process" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  expect( t.alice.trace< daofund::actions::process >( 100 ),
          "contract not initialized" );

  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.add_contributions( 1 );

  CHECK( t.get_table_size< daofund::contribution_table_type >() == 3 );

  expect( t.alice.trace< daofund::actions::process >( 1 ),
          "not ready to process" );

  t.skip_to( "2023-10-03T00:00:00.000" );

  t.alice.act< daofund::actions::process >( 1 );

  auto stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                       daofund::stakeprocess_variant >();
  CHECK(
      std::holds_alternative< daofund::stakeprocess_count >( stakeprocess ) );

  t.alice.act< daofund::actions::process >( 2 );

  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                  daofund::stakeprocess_variant >();
  auto *process_allocate =
      std::get_if< daofund::stakeprocess_allocate >( &stakeprocess );

  CHECK( process_allocate != nullptr );
  CHECK( process_allocate->contribution_date ==
         eosio::time_point_sec{ 1696118400 + 2 * 86400 } );
  CHECK( process_allocate->total_pbtc_contributed ==
         s2a( "0.300000000 PBTC" ) );
  CHECK( process_allocate->total_amount_to_allocate ==
         s2a( "10000000.0000 LIBRE" ) );

  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 2 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 1 );

  CHECK( t.get_table_size< libre::stake_table >( "stake.libre"_n ) == 3 );

  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                  daofund::stakeprocess_variant >();
  auto *process_link =
      std::get_if< daofund::stakeprocess_link >( &stakeprocess );

  CHECK( process_link != nullptr );
  CHECK( t.get_token_balance( "dao.libre"_n,
                              "btc.ptokens"_n,
                              eosio::symbol_code{ "PBTC" } ) ==
         s2a( "0.300000000 PBTC" ) );

  t.alice.act< daofund::actions::process >( 2 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 1 );
  t.chain.start_block();

  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                  daofund::stakeprocess_variant >();

  // internally moved to stakeprocess_claim but contract detects it is not time yet
  // to claim stakes so it moves to stakeprocess_standby right away

  auto *process_standby =
      std::get_if< daofund::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );

  auto contr_alice = t.get_table_object< daofund::contribution_table_type,
                                         daofund::contribution >( 0 );
  auto contr_bob = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 1 );
  auto contr_pip = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 2 );

  auto stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );
  auto stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  auto stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_index == stake_alice.index );
  CHECK( contr_bob.stake_index == stake_bob.index );
  CHECK( contr_pip.stake_index == stake_pip.index );

  CHECK( contr_alice.stake_date == stake_alice.stake_date );
  CHECK( contr_bob.stake_date == stake_bob.stake_date );
  CHECK( contr_pip.stake_date == stake_pip.stake_date );

  CHECK( contr_alice.stake_payout == stake_alice.payout );
  CHECK( contr_bob.stake_payout == stake_bob.payout );
  CHECK( contr_pip.stake_payout == stake_pip.payout );

  CHECK( contr_alice.status == daofund::e_stake_status::STAKE_LINKED );
  CHECK( contr_bob.status == daofund::e_stake_status::STAKE_LINKED );
  CHECK( contr_pip.status == daofund::e_stake_status::STAKE_LINKED );

  expect( t.alice.trace< daofund::actions::process >( 1 ),
          "not ready to process" );

  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 1 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );
  t.skip_to( "2024-03-31T00:00:01.000" );

  expect( t.alice.trace< daofund::actions::withdraw >( 0 ),
          "contribution is not ready to be claimed" );

  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 1 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 1 );

  expect( t.alice.trace< daofund::actions::withdraw >( 3 ),
          "contribution not found" );

  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                  daofund::stakeprocess_variant >();
  auto *process_claim =
      std::get_if< daofund::stakeprocess_claim >( &stakeprocess );

  CHECK( process_claim != nullptr );

  // NOTE: removed "stake" required permission from stake.libre contract for testing purposes
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 5 );

  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                  daofund::stakeprocess_variant >();
  process_standby =
      std::get_if< daofund::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 0 );
  contr_bob = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 1 );
  contr_pip = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 2 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );

  CHECK( contr_alice.status == daofund::e_stake_status::CLAIMED );
  CHECK( contr_bob.status == daofund::e_stake_status::CLAIMED );
  CHECK( contr_pip.status == daofund::e_stake_status::CLAIMED );

  CHECK( stake_alice.status == libre::stake_status::STAKE_COMPLETED );
  CHECK( stake_bob.status == libre::stake_status::STAKE_COMPLETED );
  CHECK( stake_pip.status == libre::stake_status::STAKE_COMPLETED );

  t.skip_to( "2024-04-01T00:00:00.000" );

  t.alice.act< daofund::actions::process >( 50 );

  expect( t.alice.trace< daofund::actions::process >( 100 ),
          "nothing to process" );
  expect( t.bob.trace< daofund::actions::withdraw >( 0 ),
          "missing authority of alice" );

  t.alice.act< daofund::actions::withdraw >( 0 );
  t.bob.act< daofund::actions::withdraw >( 1 );
  t.pip.act< daofund::actions::withdraw >( 2 );

  CHECK( contr_alice.stake_payout == stake_alice.payout );
  CHECK( t.get_token_balance( "alice"_n ) == contr_alice.stake_payout );

  CHECK( contr_bob.stake_payout == stake_bob.payout );
  CHECK( t.get_token_balance( "bob"_n ) == contr_bob.stake_payout );

  CHECK( contr_pip.stake_payout == stake_pip.payout );
  CHECK( t.get_token_balance( "pip"_n ) == contr_pip.stake_payout );
}

TEST_CASE( "validate allocation amounts" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );
  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.100000000 PBTC" ),
                                        "add contribute" );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  CHECK( t.get_table_size< daofund::contribution_table_type >() == 1 );
  CHECK( t.get_table_size< libre::stake_table >( "stake.libre"_n ) == 1 );

  auto contr_alice = t.get_table_object< daofund::contribution_table_type,
                                         daofund::contribution >( 0 );
  auto stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "10000000.0000 LIBRE" ) );
  CHECK( stake_alice.libre_staked == s2a( "10000000.0000 LIBRE" ) );

  t.add_contributions( 1 );
  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 1 );
  auto contr_bob = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 2 );
  auto contr_pip = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 3 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  auto stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );
  auto stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 3,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "3333333.3333 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "3333333.3333 LIBRE" ) );
  CHECK( contr_pip.stake_amount == s2a( "3333333.3333 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "3333333.3333 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "3333333.3333 LIBRE" ) );
  CHECK( stake_pip.libre_staked == s2a( "3333333.3333 LIBRE" ) );

  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "daofnd.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "add contribute" );
  t.bob.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "daofnd.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "add contribute" );

  t.skip_to( "2023-10-05T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 4 );
  contr_bob = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 5 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 4,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 5,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "5000000.0000 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "5000000.0000 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "5000000.0000 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "5000000.0000 LIBRE" ) );

  // no contributions in a day
  t.skip_to( "2023-10-06T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  // allocation is sent to the dao.libre contract
  CHECK(
      t.get_token_balance( "dao.libre"_n ) ==
      s2a(
          "10000000.0001 LIBRE" ) ); // 10000000.0001 LIBRE = 10000000.0000 LIBRE - 3333333.3333 LIBRE * 3 + 10000000.0000 LIBRE

  t.skip_to( "2023-10-07T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  CHECK(
      t.get_token_balance( "dao.libre"_n ) ==
      s2a(
          "20000000.0001 LIBRE" ) ); // 20000000.0001 LIBRE = 10000000.0001 LIBRE + 10000000.0000 LIBRE

  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.100000000 PBTC" ),
                                        "add contribute" );
  t.bob.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.200000000 PBTC" ),
                                        "add contribute" );
  t.pip.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.300000000 PBTC" ),
                                        "add contribute" );
  t.egeon.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.400000000 PBTC" ),
                                        "add contribute" );
  t.bertie.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "daofnd.libre"_n,
                                        s2a( "0.500000000 PBTC" ),
                                        "add contribute" );
  t.skip_to( "2023-10-08T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 6 );
  contr_bob = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 7 );
  contr_pip = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 8 );
  auto contr_egeon = t.get_table_object< daofund::contribution_table_type,
                                         daofund::contribution >( 9 );
  auto contr_bertie = t.get_table_object< daofund::contribution_table_type,
                                          daofund::contribution >( 10 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 6,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 7,
                                                              "stake.libre"_n );
  stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 8,
                                                              "stake.libre"_n );
  auto stake_egeon =
      t.get_table_object< libre::stake_table, libre::stake >( 9,
                                                              "stake.libre"_n );
  auto stake_bertie =
      t.get_table_object< libre::stake_table, libre::stake >( 10,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "666666.6666 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "1333333.3333 LIBRE" ) );
  CHECK( contr_pip.stake_amount == s2a( "2000000.0000 LIBRE" ) );
  CHECK( contr_egeon.stake_amount == s2a( "2666666.6666 LIBRE" ) );
  CHECK( contr_bertie.stake_amount == s2a( "3333333.3333 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "666666.6666 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "1333333.3333 LIBRE" ) );
  CHECK( stake_pip.libre_staked == s2a( "2000000.0000 LIBRE" ) );
  CHECK( stake_egeon.libre_staked == s2a( "2666666.6666 LIBRE" ) );
  CHECK( stake_bertie.libre_staked == s2a( "3333333.3333 LIBRE" ) );
}

TEST_CASE( "allocation link with already existing stakes" ) {
  tester t;

  t.fund_accounts();
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "alice"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "bob"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "pip"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );
  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:120" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.add_contributions( 1 );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  auto contr_alice = t.get_table_object< daofund::contribution_table_type,
                                         daofund::contribution >( 0 );
  auto contr_bob = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 1 );
  auto contr_pip = t.get_table_object< daofund::contribution_table_type,
                                       daofund::contribution >( 2 );

  CHECK( contr_alice.stake_index == 3 );
  CHECK( contr_bob.stake_index == 4 );
  CHECK( contr_pip.stake_index == 5 );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:120" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );

  t.chain.start_block();
  t.add_contributions( 1 );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 100 );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 3 );
  contr_bob = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 4 );
  contr_pip = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 5 );

  CHECK( contr_alice.stake_index == 12 );
  CHECK( contr_bob.stake_index == 13 );
  CHECK( contr_pip.stake_index == 14 );

  t.chain.start_block();
  t.add_contributions( 1 );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.skip_to( "2023-10-05T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 200 );

  contr_alice = t.get_table_object< daofund::contribution_table_type,
                                    daofund::contribution >( 6 );
  contr_bob = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 7 );
  contr_pip = t.get_table_object< daofund::contribution_table_type,
                                  daofund::contribution >( 8 );

  CHECK( contr_alice.stake_index == 16 );
  CHECK( contr_bob.stake_index == 17 );
  CHECK( contr_pip.stake_index == 18 );

  auto  stakeprocess = t.get_singleton< daofund::stakeprocess_singleton_type,
                                       daofund::stakeprocess_variant >();
  auto *process_standby =
      std::get_if< daofund::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );
  CHECK( process_standby->next_index_cache == 9 );
  CHECK( process_standby->next_stake_index == 19 );
}

TEST_CASE( "full contribution - even" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );
  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.complete_contributions( { "alice"_n, "bob"_n, "pip"_n, "egeon"_n } );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "daofnd.libre"_n,
                                                  s2a( "0.100000000 PBTC" ),
                                                  "add contribute on day " ),
          "contribution period has ended" );

  auto global =
      t.get_singleton< daofund::global_singleton_type, daofund::global >();

  CHECK( global.start_date == eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  CHECK( global.end_date == eosio::time_point_sec{ 1696118400 + 61 * 86400 } );
  CHECK( global.allocation_per_day == s2a( "10000000.0000 LIBRE" ) );
  CHECK( global.total_amount == s2a( "600000000.0000 LIBRE" ) );
  CHECK( global.total_amount_allocated == s2a( "600000000.0000 LIBRE" ) );

  // total contributions: 60 days of contributions period * 4 contributions (alice, bob, pip, egeon)
  // per day = 240
  t.skip_to( "2024-07-01T00:00:00.000" );

  t.alice.act< daofund::actions::process >( 100 );
  t.alice.act< daofund::actions::process >( 500 );

  std::vector< eosio::name > contributors = { "alice"_n,
                                              "bob"_n,
                                              "pip"_n,
                                              "egeon"_n };

  for ( uint8_t contr_index = 0; contr_index < 240; ) {
    for ( auto account : contributors ) {
      t.chain.as( account ).act< daofund::actions::withdraw >( contr_index );
      ++contr_index;
      t.chain.start_block();
    }
  }
}

TEST_CASE( "full contribution - odd" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );
  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.complete_contributions();

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "daofnd.libre"_n,
                                                  s2a( "0.100000000 PBTC" ),
                                                  "add contribute on day " ),
          "contribution period has ended" );

  auto global =
      t.get_singleton< daofund::global_singleton_type, daofund::global >();

  CHECK( global.start_date == eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  CHECK( global.end_date == eosio::time_point_sec{ 1696118400 + 61 * 86400 } );
  CHECK( global.allocation_per_day == s2a( "10000000.0000 LIBRE" ) );
  CHECK( global.total_amount == s2a( "600000000.0000 LIBRE" ) );
  CHECK( global.total_amount_allocated == s2a( "599999999.9940 LIBRE" ) );
}

TEST_CASE( "frozen contribution" ) {
  tester t;

  t.fund_accounts();
  t.btc_ptokens.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "btc.ptokens"_n,
                                        "alice"_n,
                                        s2a( "1000.000000000 PBTC" ),
                                        "fund account" );
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );
  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.with_code( "btc.ptokens"_n );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        daofund_name,
                                        s2a( "1000.000000000 PBTC" ),
                                        "add contribute on day" );
  t.bob.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        daofund_name,
                                        s2a( "0.000000001 PBTC" ),
                                        "add contribute on day" );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< daofund::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< daofund::actions::process >( 200 );

  auto contribution = t.get_contribution( 0 );
  auto contribution_frozen = t.get_contribution( 1 );

  CHECK( contribution.status == daofund::e_stake_status::STAKE_LINKED );
  CHECK( contribution_frozen.status == daofund::e_stake_status::FROZEN );
}

#endif