#pragma once

#include <eosio/tester.hpp>

#include <daofund.hpp>
#include <stakingtoken.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

eosio::name daofund_name{ "daofnd.libre"_n };

void daofund_setup( test_chain &t ) {
  t.create_code_account( daofund_name );
  t.set_code( daofund_name, "daofund.wasm" );
}

void dao_setup( test_chain &t ) {
  t.create_code_account( "dao.libre"_n );
  // t.set_code( "dao.libre"_n, "dao.wasm" );
}

void staking_setup( test_chain &t ) {
  t.create_code_account( "stake.libre"_n );
  t.set_code( "stake.libre"_n, "stakingtoken.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "usdt.ptokens"_n );
  t.create_code_account( "btc.ptokens"_n );

  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "usdt.ptokens"_n, "token.wasm" );
  t.set_code( "btc.ptokens"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "10000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "10000000000.0000 LIBRE" ),
                                     "" );

  t.as( "usdt.ptokens"_n )
      .with_code( "usdt.ptokens"_n )
      .act< token::actions::create >( "usdt.ptokens"_n,
                                      s2a( "1000000000.000000000 PUSDT" ) );
  t.as( "usdt.ptokens"_n )
      .with_code( "usdt.ptokens"_n )
      .act< token::actions::issue >( "usdt.ptokens"_n,
                                     s2a( "231902.183285165 PUSDT" ),
                                     "" );

  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "21000000.000000000 PBTC" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "21000000.000000000 PBTC" ),
                                     "" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "10000000000.0000 OTHER" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "10000000000.0000 OTHER" ),
                                     "" );
}

struct tester {
  test_chain   chain;
  user_context daofund = chain.as( daofund_name );
  user_context stakelibre = chain.as( "stake.libre"_n );
  user_context eosio_token = chain.as( "eosio.token"_n );
  user_context btc_ptokens = chain.as( "btc.ptokens"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    daofund_setup( chain );
    dao_setup( chain );
    staking_setup( chain );
    token_setup( chain );

    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                   account,
                                                   s2a( "1000000.0000 OTHER" ),
                                                   "fund account" );

      btc_ptokens.with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "100.000000000 PBTC" ),
                                            "fund account" );
    }

    eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "stake.libre"_n,
                                                 s2a( "9000000000.0000 LIBRE" ),
                                                 "testing:60" );
  }

  void setup_staking_contract( std::string_view &&date ) {
    skip_to( date );

    stakelibre.act< libre::actions::init >( eosio::time_point_sec{ 1696118400 },
                                            365 );
  }

  void init_contract( eosio::time_point_sec &&start_time ) {
    eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 daofund_name,
                                                 s2a( "600000000.0000 LIBRE" ),
                                                 "funding account" );
    daofund.act< daofund::actions::init >( start_time, 60 );
  }

  void add_contributions( uint8_t                    days,
                          std::vector< eosio::name > contributors =
                              { "alice"_n, "bob"_n, "pip"_n } ) {
    auto skip_day = 86400 * 1000;

    for ( uint8_t day = 0; day < days; ++day ) {
      for ( auto account : contributors ) {
        chain.as( account )
            .with_code( "btc.ptokens"_n )
            .act< token::actions::transfer >( account,
                                              daofund_name,
                                              s2a( "0.100000000 PBTC" ),
                                              "add contribute on day " );
      }

      if ( days > 1 ) {
        chain.start_block( skip_day );
      }
    }
  }

  void complete_contributions( std::vector< eosio::name > contributors =
                                   { "alice"_n, "bob"_n, "pip"_n } ) {
    auto skip_day = 86400 * 1000;

    for ( uint8_t i = 0; i < 60; ++i ) {
      add_contributions( 1, contributors );
      chain.start_block( skip_day );
      alice.act< daofund::actions::process >( 100 );
      chain.start_block();
      alice.act< daofund::actions::process >( 100 );
    }
  }

  template < typename SingletonType, typename ReturnType >
  ReturnType get_singleton( eosio::name code = daofund_name ) {
    SingletonType sing{ code, code.value };

    return sing.get();
  }

  template < typename TableType, typename ObjectType >
  ObjectType get_table_object( uint64_t    index,
                               eosio::name code = daofund_name ) {
    TableType table{ code, code.value };

    auto itr = table.find( index );

    return itr != table.end() ? *itr : ObjectType{};
  }

  auto get_token_balance( eosio::name owner,
                          eosio::name token_contract_account = "eosio.token"_n,
                          eosio::symbol_code sym_code = eosio::symbol_code{
                              "LIBRE" } ) {
    return token::contract::get_balance( token_contract_account,
                                         owner,
                                         sym_code );
  }

  daofund::contribution get_contribution( uint64_t index ) {
    daofund::contribution_table_type contribution_tb{ daofund_name,
                                                      daofund_name.value };

    auto itr = contribution_tb.find( index );

    return itr != contribution_tb.end() ? *itr : daofund::contribution{};
  }

  template < typename T >
  auto get_table_size( eosio::name code = daofund_name ) {
    T tb( code, code.value );
    return std::distance( tb.begin(), tb.end() );
  }
};