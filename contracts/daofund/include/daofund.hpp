#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

#include <stakingtoken.hpp>

#include <config.hpp>

namespace daofund {
  using stake_status_type = uint8_t;
  enum e_stake_status : stake_status_type {
    PENDING = 0,
    PENDING_LINK_STAKE,
    STAKE_LINKED,
    CLAIMED,
    WITHDRAWN,
    FROZEN
  };

  struct global {
    eosio::time_point_sec start_date;
    eosio::time_point_sec end_date;
    eosio::asset          allocation_per_day;
    eosio::asset          total_amount;
    eosio::asset          total_amount_allocated;
  };
  EOSIO_REFLECT( global,
                 start_date,
                 end_date,
                 allocation_per_day,
                 total_amount,
                 total_amount_allocated )
  using global_singleton_type = eosio::singleton< "global"_n, global >;

  struct contribution {
    uint64_t              index;
    eosio::name           account;
    eosio::asset          pbtc_amount;
    eosio::time_point_sec contr_date;
    uint64_t              stake_index;
    eosio::asset          stake_amount;
    eosio::asset          stake_payout;
    eosio::time_point_sec stake_date;
    stake_status_type     status;

    uint64_t primary_key() const { return index; }
    uint64_t by_account() const { return account.value; }
    uint64_t by_contrdate() const { return contr_date.sec_since_epoch(); }
    uint64_t by_status() const { return static_cast< uint64_t >( status ); }
  };
  EOSIO_REFLECT( contribution,
                 index,
                 account,
                 pbtc_amount,
                 contr_date,
                 stake_index,
                 stake_amount,
                 stake_payout,
                 stake_date,
                 status )
  using contribution_table_type = eosio::multi_index<
      "contribution"_n,
      contribution,
      eosio::indexed_by< "byaccount"_n,
                         eosio::const_mem_fun< contribution,
                                               uint64_t,
                                               &contribution::by_account > >,
      eosio::indexed_by< "bycontrdate"_n,
                         eosio::const_mem_fun< contribution,
                                               uint64_t,
                                               &contribution::by_contrdate > >,
      eosio::indexed_by< "bystatus"_n,
                         eosio::const_mem_fun< contribution,
                                               uint64_t,
                                               &contribution::by_status > > >;

  struct stakeprocess_standby {
    eosio::time_point_sec next_contribution_date;
    uint64_t              next_stake_index;
    uint64_t              next_index_cache;
  };
  EOSIO_REFLECT( stakeprocess_standby,
                 next_contribution_date,
                 next_stake_index,
                 next_index_cache )

  struct stakeprocess_count {
    eosio::time_point_sec contribution_date;
    uint64_t              next_index;
    eosio::asset          total_pbtc_contributed;
    uint64_t              next_stake_index;
    uint64_t              next_index_cache;
  };
  EOSIO_REFLECT( stakeprocess_count,
                 contribution_date,
                 next_index,
                 total_pbtc_contributed,
                 next_stake_index,
                 next_index_cache )

  struct stakeprocess_allocate {
    eosio::time_point_sec contribution_date;
    uint64_t              next_index;
    eosio::asset          total_pbtc_contributed =
        eosio::asset( 0, SUPPORTED_TOKEN_BTC_SYMBOL );
    eosio::asset amount_allocated =
        eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
    eosio::asset total_amount_to_allocate =
        eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
    uint64_t next_stake_index;
    uint64_t next_index_cache;
  };
  EOSIO_REFLECT( stakeprocess_allocate,
                 contribution_date,
                 next_index,
                 total_pbtc_contributed,
                 amount_allocated,
                 total_amount_to_allocate,
                 next_stake_index,
                 next_index_cache )

  struct stakeprocess_link {
    uint64_t              next_index;
    uint64_t              next_stake_index;
    eosio::time_point_sec contribution_date;
    uint64_t              next_index_cache;
    bool                  new_contrs;
  };
  EOSIO_REFLECT( stakeprocess_link,
                 next_index,
                 next_stake_index,
                 contribution_date,
                 next_index_cache,
                 new_contrs )

  struct stakeprocess_claim {
    eosio::time_point_sec contribution_date;
    uint64_t              next_stake_index;
    uint64_t              next_index_cache;
  };
  EOSIO_REFLECT( stakeprocess_claim,
                 contribution_date,
                 next_stake_index,
                 next_index_cache )

  using stakeprocess_variant = std::variant< stakeprocess_standby,
                                             stakeprocess_count,
                                             stakeprocess_allocate,
                                             stakeprocess_link,
                                             stakeprocess_claim >;
  using stakeprocess_singleton_type =
      eosio::singleton< "stakeprocess"_n, stakeprocess_variant >;

  struct distribution {
    eosio::name  account;
    eosio::asset reward_amount;
    eosio::asset total_claimed;

    uint64_t primary_key() const { return account.value; }
  };
  EOSIO_REFLECT( distribution, account, reward_amount, total_claimed )

  using distribution_table_type =
      eosio::multi_index< "distribution"_n, distribution >;

  class daofund : public eosio::contract {
  public:
    using contract::contract;

    daofund( eosio::name                       receiver,
             eosio::name                       code,
             eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ),
          global_sing( receiver, receiver.value ),
          stakeprocess_sing( receiver, receiver.value ),
          contribution_tb( receiver, receiver.value ) {}

    void notify_transfer( eosio::name  &from,
                          eosio::name  &to,
                          eosio::asset &quantity,
                          std::string  &memo );

    void init( eosio::time_point_sec &start_date, uint8_t total_days );
    void process( uint16_t limit );
    void withdraw( uint64_t id );

  private:
    global_singleton_type       global_sing;
    stakeprocess_singleton_type stakeprocess_sing;
    contribution_table_type     contribution_tb;

    void set_global( eosio::asset &quantity );
    void add_contribution( eosio::name &account, eosio::asset &quantity );
    void process_count( uint16_t &limit, stakeprocess_count &stakeprocess );
    void process_allocate( uint16_t              &limit,
                           stakeprocess_allocate &stakeprocess ); // stake
    void process_link( uint16_t &limit, stakeprocess_link &stakeprocess );
    void process_claim( uint16_t &limit, stakeprocess_claim &stakeprocess );
    eosio::asset
    calculate_allocation_reward( const eosio::asset &pbtc_contributed,
                                 const eosio::asset &total_pbtc_contributed,
                                 const eosio::asset &total_allocation_per_day );
    void
         send_leftofver_allocation( const eosio::asset          &amount,
                                    const eosio::time_point_sec &contribution_date );
    void send_daily_pbtc_contribution(
        const eosio::asset          &amount,
        const eosio::time_point_sec &contribution_date );
    void add_balance( const eosio::name  &account,
                      const eosio::asset &quantity );
    void sub_balance( const eosio::name  &account,
                      const eosio::asset &quantity );
  };

  EOSIO_ACTIONS( daofund,
                 "daofnd.libre"_n,
                 action( init, start_date, total_days ),
                 action( process, limit ),
                 action( withdraw, id ),
                 notify( SUPPORTED_TOKEN_LIBRE_CONTRACT, transfer ),
                 notify( SUPPORTED_TOKEN_BTC_CONTRACT, transfer ) )
} // namespace daofund